
var id;
var backgroundimage1;
var header1;
var header2;
var textbody;
var buttonimage;


$.mynamespace = {};
$.mynamespace.index = 0;
$.mynamespace.ids = new Array();
$.mynamespace.backgroundimage1s= new Array();
$.mynamespace.productimages = new Array();
$.mynamespace.header1s = new Array();
$.mynamespace.header2s = new Array();
$.mynamespace.textbodies = new Array();
$.mynamespace.subtitlecolors = new Array();
$.mynamespace.bodycolors = new Array();
$.mynamespace.titlecolors = new Array();

$.mynamespace.buttonimages = new Array();
$.mynamespace.buttontargeturls = new Array();

$('#clock').hide();
$('#previous').hide();
$('#next').hide();


function loadAd()
	{
	$('#clock').hide();
	$('#previous').hide();
	$('#next').hide(); 
  	
	if ($.mynamespace.ids.length >  1)
	{
	    $('#clock').show();
	    $('#previous').show();
	    $('#next').show();
	  
	}
	  
	$('#animationbody').hide().delay(1400);
				 
				
	// slide the background image down and then slide the center container image up
	$('#animationbody').fadeIn('slow').delay(1400);
				
				
				
				
	
	  /* $('<div class="slideshowitem" id="link_'+id+'"></div>').html('<a href="'+id+'">'+id+'</a>').appendTo('#animationbody'); */
	  $('').replaceAll('.header1');
	  $('').replaceAll('.header2');
	  $('').replaceAll('.textbody');
	  $('').replaceAll('.buttonimage');
	  $('').replaceAll('.product');
	  
	  
	  $('#animationbody').css('background-image','url("' + $.mynamespace.backgroundimage1s[$.mynamespace.index] + '")');/*.html('<img src="'+backgroundimage1+'"/>').appendTo('#animationbody');*/
	  $('#animationbody > #rightcontainer').append('<div class="header1">' + $.mynamespace.header1s[$.mynamespace.index] + '</div>');
	  $('#animationbody > #rightcontainer').append('<div class="header2">' + $.mynamespace.header2s[$.mynamespace.index] + '</div>');
	  $('#animationbody > #rightcontainer').append('<div class="textbody">' + $.mynamespace.textbodies[$.mynamespace.index] + '</div>');
	  $('#animationbody > #rightcontainer').append('<div class="buttonimage"><a href="' + $.mynamespace.buttontargeturls[$.mynamespace.index] + '"><img border="0" src="' + $.mynamespace.buttonimages[$.mynamespace.index]  + '"/></a></div>');
	  $('#animationbody > #centercontainer').append('<div class="product"><img class="product" src="' + $.mynamespace.productimages[$.mynamespace.index]  + '"/></div>');
				    
				  
			  
	  $('#animationbody > #centercontainer > .product').hide();
	  $('#animationbody > #centercontainer > .product').fadeIn('slow');
				  
	  $('#animationbody > #rightcontainer > .header1').hide().delay(2000);

	  $('#animationbody > #rightcontainer > .header2').hide().delay(2000);
	  $('#animationbody > #rightcontainer > .header2').css('color', "#" + $.mynamespace.subtitlecolors[$.mynamespace.index]);
$('#animationbody > #rightcontainer > .header1').css('color', "#" + $.mynamespace.titlecolors[$.mynamespace.index]);
$('#animationbody > #rightcontainer > .textbody').css('color', "#" + $.mynamespace.bodycolors[$.mynamespace.index]);
	  $('#animationbody > #rightcontainer > .textbody').hide().delay(2000);
	  $('#animationbody > #rightcontainer > .buttonimage').hide().delay(2000);
			  
	  $('#animationbody > #rightcontainer > .header1').fadeIn('slow').delay(2000);
	  $('#animationbody > #rightcontainer > .header2').fadeIn('slow').delay(2000);
	  $('#animationbody > #rightcontainer > .textbody').fadeIn('slow').delay(2000);
	  $('#animationbody > #rightcontainer > .buttonimage').fadeIn('slow').delay(2000);
	  

    }


$(document).ready(function(){    
	
	$.ajax({
		type: "GET",
		url: "data.xml",
		dataType: "xml",
		
		/*
		success: function(xml) {
			
			// background slide
			$(xml).find('slide').each(function(){
				

				$.mynamespace.ids.push($(this).attr('id'));
				$.mynamespace.backgroundimage1s.push($(this).find('backgroundimage1').text());
				$.mynamespace.productimages.push($(this).find('productimage').text());
				$.mynamespace.header1s.push($(this).find('header1').text());
				$.mynamespace.header2s.push($(this).find('header2').text());
				$.mynamespace.textbodies.push($(this).find('textbody').text());
				$.mynamespace.buttonimages.push($(this).find('buttonimage').text());
				
				}
				  
			);
			*/
			success: function(xml) {
			
			// background slide
			$(xml).find('slide').each(function(){
				

				$.mynamespace.ids.push($(this).attr('id'));
				$.mynamespace.backgroundimage1s.push($(this).find('backgroundimage1').text());
				$.mynamespace.productimages.push($(this).find('productimage').text());
				$.mynamespace.header1s.push($(this).find('title').text());
				$.mynamespace.header2s.push($(this).find('subTitle').text());
				$.mynamespace.textbodies.push($(this).find('body').text());
				$.mynamespace.buttonimages.push($(this).find('buttonimage').text());
				$.mynamespace.buttontargeturls.push($(this).find('ctaURL').text());
				$.mynamespace.subtitlecolors.push($(this).find('subTitle').attr('textColor'));		
$.mynamespace.titlecolors.push($(this).find('title').attr('textColor'));		

$.mynamespace.bodycolors.push($(this).find('body').attr('textColor'));								
				}
				  
			);
			$.mynamespace.numberOfAds = 3; // number of ads
			
			loadAd();
			   
			    $('#previous').click(function(){
				$.mynamespace.index--;
				if ($.mynamespace.index == -1)
					$.mynamespace.index = $.mynamespace.numberOfAds -1;				
				$.mynamespace.index = $.mynamespace.index % $.mynamespace.numberOfAds;			
				loadAd();
			  	
			    });

			    $('#next').click(function(){
			 
				moveNext();
				
				
			   });
			function moveNext ()
			{
					$.mynamespace.index++;
				$.mynamespace.index = $.mynamespace.index % $.mynamespace.numberOfAds;
				loadAd();
			
			}
			
			// rotate ad every six seconds
			setInterval(moveNext, 9000); 
		}
	});
	
	
    
});


		
